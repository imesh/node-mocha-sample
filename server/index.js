var express = require("express");
var app = express();

app.get("/foo", function(req, res) {
    res.sendStatus(200);
});

app.get("/bar", function(req, res) {
    res.send(200, "Hello, world!");
});

module.exports = app;

var assert = require('assert'),
    http = require('http'),
    app = require("../server/index.js");

before(function() {
  var port = process.env.PORT || '3000';
  app.set('port', port);

  var server = http.createServer(app);
  server.listen(port);

  console.log("Server started on port " + port);
});

describe('/', function () {
  it('should return 200', function (done) {
    http.get('http://localhost:3000/foo', function (res) {
      assert.equal(200, res.statusCode);
      done();
    });
  });

  it('should say "Hello, world!"', function (done) {
    http.get('http://localhost:3000/bar', function (res) {
      var data = '';
      res.on('data', function (chunk) {
        data += chunk;
      });
      res.on('end', function () {
        assert.equal(200, res.statusCode);
        assert.equal('Hello, world!', data);
        done();
      });
    });
  });
});


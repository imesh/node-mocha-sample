# Node.js Mocha Sample

This is a sample Node.js application written for evaluating Mocha test framework 
features. The application exposes two API resources using Express.js and test 
those using two integration tests.

## How to run


- Execute the bellow command to start the web application:

   ```
   npm install
   npm start
   ```

- Execute the below command to test the API resources:

   ```
   curl -v http://localhost:3000/foo
   curl -v http://localhost:3000/bar
   ```

## How to execute the tests

- Stop any running web application instances and execute the below command to
run the integration tests:

   ```
   npm test
   ```
   
## License

This sample is licensed under Apache 2.0 license.
var app = require("./server/index.js");
var http = require("http");

var port = process.env.PORT || '3000';
app.set('port', port);

var server = http.createServer(app);
server.listen(port);

console.log("Server started on port " + port);